package com.example.memorygameildar.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.memorygameildar.R
import com.example.memorygameildar.databinding.ActivityMainBinding
import com.example.memorygameildar.databinding.ActivityScoreBinding

class ScoreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent=getIntent()
        var score = intent.getIntExtra("score", 0)

        println("-->"+score)

        val binding= ActivityScoreBinding.inflate(layoutInflater)
        setContentView(binding.root) //conecta layout y su codigo

        val play =binding.playAgain
        val menu=binding.Menu

        val scoreView = binding.score.setText("score " + score)


        play.setOnClickListener{
            val intent= Intent(this, GameActivity::class.java) //creas intent
            startActivity(intent) //activas intent
        }

        menu.setOnClickListener{
            val intent= Intent(this, MainActivity::class.java) //creas intent
            startActivity(intent) //activas intent
        }





    }
}