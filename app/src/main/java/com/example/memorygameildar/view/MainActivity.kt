package com.example.memorygameildar.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import com.example.memorygameildar.databinding.ActivityMainBinding



class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        val binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root) //conecta layout y su codigo

        var hardMode=modeFun(binding)

        val play =binding.playButtom
        val help=binding.helpButton
        val gameMode=binding.gameMode

        play.setOnClickListener{
            var hardMode=modeFun(binding)
            println("hardome intent->"+hardMode)
            val intent= Intent(this, GameActivity::class.java) //creas intent
            intent.putExtra("hardMode",hardMode) //pasas el boleano si el modo es dificil
            startActivity(intent)
        }

        help.setOnClickListener{
            val alertDialog=AlertDialog.Builder(this)
            alertDialog.setTitle("HELP")
            alertDialog.setMessage("You need to collect the maximum pair of cards")
            alertDialog.setNeutralButton("close",null)
            alertDialog.create().show()
            }



        gameMode.setOnClickListener{
           if (binding.gameMode.getText().toString()=="Easy") {
               gameMode.setText("Hard")

           }else{
               gameMode.setText("Easy")

           }

        }
    }

    private fun modeFun(binding: ActivityMainBinding): Boolean { //si es dificil decuelve

        if (binding.gameMode.getText().toString()!="Easy") { //si no es modo facil dificil true
            return true;

        }
        return false;
    }
}
