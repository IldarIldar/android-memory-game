package com.example.memorygameildar.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.ImageView
import android.widget.TextView
import com.example.memorygameildar.R
import com.example.memorygameildar.databinding.ActivityGameBinding

class GameActivity : AppCompatActivity() {

    var stop = false;

    data class TransspasPack(var move: Int, var touchedIds_Images :MutableMap<Int, Int>)

    lateinit var binding: ActivityGameBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.Theme_AppCompat)

        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_game)
        binding=ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root) //conecta layout y su codigo

        var hardMode = intent.getBooleanExtra("hardMode", false)
        println("HARDMODE --->"+hardMode)
        var miliseconds:Long=15000
        if (hardMode){
            miliseconds=6000
        }

        var timerView=binding.timer
        val timer_object = object: CountDownTimer(miliseconds, 1000){
            override fun onTick(p0: Long) {
                timerView.setText("Faltan "+p0/1000)
            }

            override fun onFinish() {
                println("pasamos")
                passToNextActivity()
            }

        }.start()

        var counter=binding.movesCounter
        setView(counter,0.toString())

        val image1: ImageView = binding.image1
        val image2: ImageView = binding.image2
        val image3: ImageView = binding.image3
        val image4: ImageView = binding.image4
        val image5: ImageView = binding.image5
        val image6: ImageView = binding.image6
        val imageArray = arrayOf(image1, image2, image3, image4, image5, image6)


        val piecesId = randomCards() //las tres cartas
        val finalPiecesId = arrayOf(piecesId[0],piecesId[1],piecesId[3],piecesId[0],piecesId[1],piecesId[3]) //duplicamos las 3 cartas
        finalPiecesId.shuffle() //mezclamos las 3 cartas

        //playGame(imageArray, finalPiecesId)

        //Aqui el juego
        var move=0;

        //cada turno antes de comprovar son dos movimientos por eso creo una mutableMap
        //para transpasar la id y la img view de primer turno en key 1 y 2 respectivamente
        // y la del segundo movimiento en 3 y 4, para la funcion guess
        var touchedIds_Images= mutableMapOf(1 to -1, 2 to -1, 3 to -1, 4 to -1) //[1]id [1]img [2]id [2]img

        //este objeto sirve para transpasar varios datos en un movimiento
        var movePack = TransspasPack(move, touchedIds_Images)



            imageArray[0].setOnClickListener {
                if(!stop) {
                    movePack = move(movePack, finalPiecesId[0], 0, imageArray)
                }
            }

            imageArray[1].setOnClickListener {
                if(!stop) {
                    movePack = move(movePack, finalPiecesId[1], 1, imageArray)
                }
            }

            imageArray[2].setOnClickListener {
                if(!stop) {
                    movePack = move(movePack, finalPiecesId[2], 2, imageArray)
                }
            }

            imageArray[3].setOnClickListener {
                if(!stop) {
                    movePack = move(movePack, finalPiecesId[3], 3, imageArray)
                }

            }

            imageArray[4].setOnClickListener {
                if(!stop) {
                    movePack = move(movePack, finalPiecesId[4], 4, imageArray)
                }

            }

            imageArray[5].setOnClickListener {
                if(!stop) {
                    movePack = move(movePack, finalPiecesId[5], 5, imageArray)
                }
            }


        var stopButton=binding.stopButtom

        stopButton.setOnClickListener{

            stop = !stop
            println("la variable stop ahora es "+stop)
            if(!stop){
                binding.stopButtom.setImageResource(R.drawable.ic_baseline_pause_24)
                timer_object.start()
            }else{
                binding.stopButtom.setImageResource(R.drawable.ic_baseline_play_arrow_24)
                timer_object.cancel()
            }
        }

    }

    //random generated cards
    fun randomCards(): Array<Int> {

        val piecesId =
            arrayOf(0, 1, 2, 3, 4, 5) //1:pawn, 2:bishop, 3:knight, 4:tower, 5:queen, 6:king
        piecesId.shuffle() //mezclamos
        var finalPiecesId = mutableListOf<Int>();
        for (i in 0..piecesId.size - 1) {

            finalPiecesId.add(piecesId[i])

        }

        return finalPiecesId.toTypedArray()
    }

    fun pieceAssignation( //esta funcion pone la imagen al ImageView a traves del array de IDs
        pieceId: Int,
        image: ImageView
    ) { //aqui asignaremos la imagen a la pieza
        when (pieceId) {
            0 -> image.setImageResource(R.drawable.pawn)
            1 -> image.setImageResource(R.drawable.bishop)
            2 -> image.setImageResource(R.drawable.knight)
            3 -> image.setImageResource(R.drawable.tower)
            4 -> image.setImageResource(R.drawable.queen)
            5 -> image.setImageResource(R.drawable.king)
            else -> image.setImageResource(R.drawable.misterio)
        }
    }


    fun move(
        transspasPack: TransspasPack, //cada movimiento
        pieceId: Int,
        imageId: Int,
        image: Array<ImageView>
    ):TransspasPack{   //[1]id [1]img [2]id [2]img


        var move=transspasPack.move
        var touchedIds_Images=transspasPack.touchedIds_Images

        println("--> entro al click listener: move-> "+move+" pieceId-> "+pieceId+" img ->"+imageId)


        if (move==0){//si es el primer movimiento, toma la primera casilla
            viewCardForLitteTime(pieceId,image[imageId])
            touchedIds_Images.put(1,pieceId)
            touchedIds_Images.put(2,imageId)

            move++;


        }
        else if(move == 1) { //si es el segundo movimiento, toma la segunda casilla
            //viewCardForLitteTime(finalPiecesId[0],imageArray[0])
            viewCardForLitteTime(pieceId,image[imageId])

            touchedIds_Images.put(3,pieceId)
            touchedIds_Images.put(4,imageId)

            guessOrNot(image,touchedIds_Images) //vemos si las dos imagenes eran la misma
            move =0
        }

        if (txViewToInt(binding.movesCounter)==3) {
            println("pasamos")
            passToNextActivity()
        }


        transspasPack.move=move
        transspasPack.touchedIds_Images=touchedIds_Images
        println("move pack-->"+transspasPack.move)

        return transspasPack
    }


    fun viewCardForLitteTime( //aqui es cuando el jugador gira la carta por primera vez
        pieceId: Int,
        image: ImageView
    ) {
        pieceAssignation(pieceId,image )
    }

    fun guessOrNot( //aqui vemos si el jugador consigue descubrir dos cartas o no
        //touchedImages: Array<Int>,
        image: Array<ImageView>,
        // touchedValues:Array<Int>
        touchedIds_Images: MutableMap<Int, Int>

    ) {
        val binding = ActivityGameBinding.inflate(layoutInflater)

        println("touched images -->"+touchedIds_Images.get(2)+"  "+touchedIds_Images.get(4))
        println("touched val -->"+touchedIds_Images.get(1)+"  "+touchedIds_Images.get(3))

        if (touchedIds_Images.get(1)==touchedIds_Images.get(3)){ //si son iguales
            println(":D guess")


            sumPoint()

        }else{ //girar y esconder, pierde

            println("dont guess")

            pieceAssignation(-1,image[touchedIds_Images.getValue(2)]) //luego escondemos
            pieceAssignation(-1,image[touchedIds_Images.getValue(4)])
        }

    }


    private fun sumPoint() { //+1 pint

        var txToInt=txViewToInt(binding.movesCounter)+1
        println(txToInt)
        setView(binding.movesCounter,txToInt.toString())

    }

    private fun txViewToInt(txView: TextView): Int {

        var string=txView.getText().toString()
        var num=string.toInt()
        return num
    }

    private fun setView(txView: TextView,value: String ){

        var view=txView
        view.setText(value)
    }

    private fun passToNextActivity() {

        var segundosString=binding.timer.getText()
        var segundosNum=segundosString.subSequence(6,segundosString.length).toString().trim()
        var score =txViewToInt(binding.movesCounter)*segundosNum.toInt()

        val intent= Intent(this, ScoreActivity::class.java) //creas intent
        intent.putExtra("score",score)
        startActivity(intent) //activas intent

    }

}